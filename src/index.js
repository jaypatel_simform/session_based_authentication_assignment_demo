const express = require("express");
const app = express();
const path = require("path");
const helmet = require("helmet");
const cookieparser = require("cookie-parser");
const sessions = require('express-session');
app.use(sessions({
    secret: "this is my session secret token",
    saveUninitialized: true,
    cookie: { maxAge: 1000*60 },
    resave: false,
}));               

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://127.0.0.1:5500");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
app.use(helmet());
app.use(cookieparser());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "..", "public")));
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));

const PORT = process.env.PORT || 5000;
var session;
app.get("/", (req, res) => {
    session = req.session;
    if (session.uid){
    const username = session.uid
        return res.render("home", { username });}
    else
        return res.render("login")
});

app.get("/login", (req, res) => {
        return res.render("login");
});

app.get("/welcome", (req, res) => {
    let username = req.session.uid;
    console.log("welcome Req session :",req.session)

    if(!username)
    return res.render("login",{error:"you must have to login.."})
    return res.render("welcome", {
        username,
    });
});

app.post("/process_login", (req, res) => {
    let { username, password } = req.body;

    let userdetails =[ {
        username: "jay",
        password: "123456",
    },{
        username:"meet",
        password:"123456"
    }
];
    //console.log("data", req.body)
   const user = userdetails.filter((user)=> user.username===username && user.password === password)
    if (user) {
        console.log("login Req session :",req.session)
        session = req.session;
        session.uid = req.body.username;
        console.log("Session id :",session.id)
        //res.session = session       
        return res.redirect("/welcome");
    } else {
        // redirect with a fail msg
        return res.render("login",{error:"invalid credentials.."});
    }
});  
         
app.get('/public',(req,res)=>{
    console.log("CORS : ",req.query.name)
    res.send({name:"This all is public contents"})
})
app.get("/logout", (req, res) => {
//    console.log("before delete session details : ",req.session.sid)
    req.session.destroy()
    return res.redirect("/");
});

app.listen(PORT, () => console.log(`server started on port: ${PORT}`));
